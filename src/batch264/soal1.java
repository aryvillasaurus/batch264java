package batch264;

import java.util.Scanner;

public class soal1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan Nominal Pulsa : ");
		int pulsa = input.nextInt();
		
		if (pulsa >= 10000 && pulsa < 25000) {
			System.out.println("Point : 80");
		} else if (pulsa >= 25000 && pulsa < 50000) {
			System.out.println("Point : 200");
		} else if (pulsa >= 50000 && pulsa < 100000) {
			System.out.println("Point : 400");	
		} else if (pulsa >= 100000) {
			System.out.println("Point : 800");
		} else {
			System.out.println("Tidak mendapat Point");
		}		
		input.close();
	}

}
