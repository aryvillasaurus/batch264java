package day2;
import java.util.*;

public class soal7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//menggunakan for
//	Scanner input = new Scanner(System.in);	
//	int n = 0;
//	int angka = 2; 
//	
//	System.out.println("Masukan n : ");
//	n = input.nextInt();
//	
//	for(int i = 0; i < n; i++) {
//	     System.out.print(angka + " ");
//	     angka*=2;
//	  }
//	  System.out.print(" ");
		
		//menggunakan array loop
		Scanner input = new Scanner(System.in);	
		int angka = 2;
		
		System.out.println("Masukan n : ");
		int n = input.nextInt();
			
		int[] nilaiDeret = new int[n];
			
		for(int i = 0; i < n; i++) {
			nilaiDeret[i] = angka;
			angka*=2;
			System.out.print(nilaiDeret[i] + " ");
		}

	}
}
