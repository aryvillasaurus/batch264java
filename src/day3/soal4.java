package day3;

import java.util.*;
import java.time.*;
import java.text.*;

public class soal4 {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub
		Scanner scan = new Scanner(System.in);
		System.out.print("Waktu = ");
		String input = scan.nextLine();
		SimpleDateFormat tanggal = new SimpleDateFormat("hh:mm:ssaa"); // mendefinisikan format waktu 12 jam
		SimpleDateFormat outputformat = new SimpleDateFormat("HH:mm:ss");// mengubah format ke 24 jam
		Date waktu = null;
		String str = "";

		waktu = tanggal.parse(input); // mengkonversi string ke waktu
		str = outputformat.format(waktu); // Mengubah format ke waktu dan menyimpan kedalam string
		System.out.println("Waktu = " + str);// Menampilkan Waktu

	}

}
