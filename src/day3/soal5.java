package day3;

import java.util.Scanner;

public class soal5 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan N : ");
		int nilai = input.nextInt();

		int nilaiBaru = nilai;
		System.out.println("Faktor-faktornya:");
		for (int i = 1; i <= nilai; i++) {
			for (int j = 2; j <= nilai; j++) {
				if (nilaiBaru % j == 0) {
					System.out.println(nilaiBaru + "/" + j + "=" + (nilaiBaru / j));
					nilaiBaru /= j;
					break;
				}
			}
		}
		input.close();
	}
}
