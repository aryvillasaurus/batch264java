package day3;

import java.util.Arrays;
import java.util.Scanner;

public class soal7 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.println("SOS : ");
		String sinyal = input.nextLine().toLowerCase();
		char[] cek = new char[sinyal.length()];

		int n = sinyal.length();
		int tampung = n / 3;

		for (int i = 0; i < n; i++) {
			cek[i] = sinyal.charAt(i);
		}
		for (int i = 0; i < n; i += 3) {
			if (cek[i] == 's' && cek[i + 1] == 'o' && cek[i + 2] == 's') {
				tampung--;
			}
		}
		System.out.println("Total sinyal salah : " + tampung);
	}

}
