package day3array2dm;

import java.util.*;

public class excel1 {

	public static void main(String[] args) {

		int uangbayar = 0;
		int belanja = 0;
		int hargatotal = 0;
		Scanner input = new Scanner(System.in);

		System.out.print("Masukan Uang Pengunjung : ");
		uangbayar = input.nextInt();

		System.out.println("");
		System.out.println("=====================");
		System.out.print("Masukkan Jumlah Baju: ");

		int baju = input.nextInt();
		int hargabaju[] = new int[baju];

		int[] total_belanja = new int[9];

		for (int i = 0; i < baju; i++) {
			System.out.print("Masukan Harga Baju : ");
			hargabaju[i] = input.nextInt();
		}

		System.out.print("Masukkan Jumlah Celana: ");
		int celana = input.nextInt();
		int hargacelana[] = new int[celana];

		for (int i = 0; i < celana; i++) {
			System.out.print("Masukan Harga Celana: ");
			hargacelana[i] = input.nextInt();
		}

		for (int i = 0; i < hargabaju.length; i++) {
			for (int j = 0; j < hargacelana.length; j++) {
				hargatotal = hargabaju[i] + hargacelana[j];
				if (hargatotal <= uangbayar) {
					total_belanja[j] = hargatotal;
				}
				if (hargatotal <= uangbayar && hargatotal >= belanja) {
					belanja = hargatotal;
					Arrays.sort(total_belanja);
					System.out.println("=====================");
					System.out.print("Uang Belanja Maksimal : " + total_belanja[total_belanja.length - 1]);
				}
			}
		}

		input.close();
	}
}
