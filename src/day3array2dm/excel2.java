package day3array2dm;

import java.util.Scanner;

public class excel2 {
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		System.out.print("Masukan angka pada array: ");
		int n = input.nextInt();

		int array[] = new int[n];
		// Input data ke array
		for (int i = 0; i < array.length; i++) {
			System.out.print("angka ke " + (i + 1) + " : ");
			array[i] = input.nextInt();
		}
		// Menampilkan data dalam array
		System.out.println("Angka yang dimasukan ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		// rotasi
		System.out.print("Rotasi : ");
		int rotasi = input.nextInt();

		// cetak 1
		System.out.print("array awal     : ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		for (int rot = 0; rot < rotasi; rot++) {

			// geser
			int j = 0;
			int temp = array[0];
			for (j = 0; j < array.length - 1; j++) {
				array[j] = array[j + 1];
			}
			array[j] = temp;

			// cetak 2
			System.out.print("array geser " + (rot + 1) + "x : ");
			for (int i = 0; i < array.length; i++) {
				System.out.print(array[i] + ", ");
			}
			System.out.println();

		}

		input.close();
	}
}
