package day3array2dm;

import java.util.Arrays;
import java.util.Scanner;

public class excel5 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan Kata: ");
		String kata = input.nextLine();
		
		kata = kata.toLowerCase();
		
		char[] chars = kata.toCharArray();

		Arrays.sort(chars);
        String sorted = new String(chars);
		
		System.out.print("Huruf vokal yang muncul:");
		for (int i = 0; i < sorted.length(); i++) {
			if (sorted.charAt(i) == 'a' || sorted.charAt(i) == 'e' || sorted.charAt(i) == 'i' || sorted.charAt(i) == 'o'
					|| sorted.charAt(i) == 'u') {
				System.out.print("" + sorted.charAt(i));
			}
		}
		System.out.println("");
		
		System.out.print("Huruf konsonan yang muncul:");
		for (int j = 0; j < sorted.length(); j++) {	
			if (sorted.charAt(j) != 'a' && sorted.charAt(j) != 'e' && sorted.charAt(j) != 'i' && sorted.charAt(j) != 'o'
					&& sorted.charAt(j) != 'u' && sorted.charAt(j) != ' ') {
			
				System.out.print("" + sorted.charAt(j));
			}
		}

	}

}
