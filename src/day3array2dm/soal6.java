package day3array2dm;

import java.util.Scanner;

public class soal6 {
	public static void main(String args[]) {

		int nilaiAwal1 = 0;
		int nilaiAwal2 = 1;
        Scanner input = new Scanner(System.in);
    	
    	System.out.print("Masukan N : ");
		int n = input.nextInt();
		
		int baris = 3;

		int[][]  arrayAngka = new int[baris][n];
		
		for (int i = 0; i < baris; i++) {
			for(int j = 0; j < n; j++) {
				if(i == 0) {
					arrayAngka[i][j] = nilaiAwal1;
					nilaiAwal1++;
					System.out.print(arrayAngka[i][j] + " ");
				} else if (i == 1) {
					arrayAngka[i][j] = nilaiAwal2;
					nilaiAwal2*=n;
					System.out.print(arrayAngka[i][j] + " ");
				} else {
					arrayAngka[i][j] = arrayAngka[1][j]+arrayAngka[0][j];
					System.out.print(arrayAngka[i][j] + " ");
				}
			}
			System.out.println(" ");
		}

        }
}
