package day3array2dm;

import java.util.Scanner;

public class soal7 {

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        
        int nilai = 0;
        int baris = 3;
        
        
        System.out.print("Masukan N :");
        int n=input.nextInt();
        System.out.println("=============");
        
        int array[][] =new int[baris][n];
        
        for(int i=0; i<baris; i++){
            for(int j=0; j<n; j++){
                array[i][j] = nilai++;
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
    
}