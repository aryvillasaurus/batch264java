package day3test;

import java.util.Date;
import java.util.Scanner;

public class soal1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);

		System.out.println("Masukan Tanggal Masuk :");
		String tglMasuk = scan.nextLine();

		System.out.print("Masukan Jam Masuk: ");
		String[] waktuMasuk = scan.next().split("\\.");
		String jamMasuk = waktuMasuk[0];
		String menitMasuk = waktuMasuk[1];

		System.out.print("Masukan Jam Keluar: ");
		String[] waktuKeluar = scan.next().split("\\.");
		String jamKeluar = waktuKeluar[0];
		String menitKeluar = waktuKeluar[1];

		int jamMas = Integer.parseInt(jamMasuk);
		int menitMas = Integer.parseInt(menitMasuk);
		int jamKel = Integer.parseInt(jamKeluar);
		int menitKel = Integer.parseInt(menitKeluar);

		int jamParkir = jamKel - jamMas;
		int menitParkir = menitKel - menitMas;
		int lamaParkir = (jamParkir * 60) + menitParkir;
		lamaParkir = lamaParkir / 60;

		System.out.println("Lama Parkir : " + lamaParkir + " Jam");

		int biayaParkir = 0;
		biayaParkir = lamaParkir * 3000;
		if (menitParkir != 0) {
			biayaParkir = biayaParkir + 3000;
		}

		System.out.println("Biaya Parkir : " + biayaParkir);

	}
}
