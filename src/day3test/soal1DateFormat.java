package day3test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class soal1DateFormat {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		System.out.println("Masukan Tanggal Masuk :");
		String tglMasuk = input.nextLine();

		System.out.print("Masukan Jam Masuk: ");
		String waktuMasuk = input.next();
		System.out.print("Masukan Jam Keluar: ");
		String waktuKeluar = input.next();

		SimpleDateFormat format = new SimpleDateFormat("hh.mm.ss");
		Date mulai = format.parse(waktuMasuk+".00");
		Date keluar = format.parse(waktuKeluar+".00");
		
		int biayaParkir = 0;
		long lamaDalamMenit = (keluar.getTime()-mulai.getTime()) / (1000 * 60);
		float hitung = (float) lamaDalamMenit / 60;
		if(hitung % 1 >= 0.5) {
			biayaParkir += (int) Math.ceil(hitung) * 3000;
		}else {
			biayaParkir += hitung * 3000;
		}
		System.out.println("Biaya Parkir = "+ biayaParkir);
	}
}
