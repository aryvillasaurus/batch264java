package day3test;

import java.util.Date;
import java.util.Scanner;

public class soal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);

		System.out.print("Masukan Tanggal Pinjam: ");
		String[] pinjam = scan.next().split("-");
		String tglPinjam = pinjam[0].substring(0, 2);
		String blnPinjam = pinjam[1].substring(0, 2);
		String tahunPinjam = pinjam[2].substring(0, 4);

		System.out.print("Masukan Tanggal Kembali: ");
		String[] kembali = scan.next().split("-");
		String tglKembali = kembali[0].substring(0, 2);
		String bulanKembali = kembali[1].substring(0, 2);
		String tahunKembali = kembali[2].substring(0, 4);

		int denda = 0;
		int tglPin = Integer.parseInt(tglPinjam);
		int blnPin = Integer.parseInt(blnPinjam);
		int thnPin = Integer.parseInt(tahunPinjam);

		int tglKem = Integer.parseInt(tglKembali);
		int blnKem = Integer.parseInt(bulanKembali);
		int thnKem = Integer.parseInt(tahunKembali);

		int lamaTgl = tglKem - tglPin;
		int lamaBulan = blnKem - blnPin;
		int lamaTahun = thnKem - thnPin;

		int lamaPinjam = lamaTgl + (lamaBulan * 30) + (lamaTahun * 365);

		if (lamaPinjam > 3) {
			denda = lamaPinjam * 500;
		} else {
			denda = 0;
		}

		System.out.println("Lama Peminjaman :  " + lamaPinjam + " Hari");
		System.out.print("Denda :  " + denda);

	}
}
