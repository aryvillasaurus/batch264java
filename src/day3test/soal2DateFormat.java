package day3test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class soal2DateFormat {
	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);

		System.out.print("Masukan Tanggal Pinjam : ");
		String tglPinjam = input.next();
		System.out.print("Masukan Tanggal Kembali: ");
		String tglKembali = input.next();

		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date pinjam = format.parse(tglPinjam);
		Date kembali = format.parse(tglKembali);
		
		int denda = 0;
		int lamaTgl = kembali.getDate() - pinjam.getDate();
		int lamaBulan = kembali.getMonth() - pinjam.getMonth();
		int lamaTahun = kembali.getYear() - pinjam.getYear(); 		
		
		int lamaPinjam = lamaTgl + (lamaBulan * 30) + (lamaTahun * 365);
		
		if (lamaPinjam > 3) {
			denda = lamaPinjam * 500;
		} else {
			denda = 0;
		}

		System.out.println("Lama Peminjaman : " + lamaPinjam + " Hari");
		System.out.print("Denda :  " + denda);
		
	}
}
