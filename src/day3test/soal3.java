package day3test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class soal3 {
	public static void main(String[] args) throws ParseException {

		Scanner input = new Scanner(System.in);

		String[] listBulan = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September",
				"Oktober", "November", "Desember" };

		System.out.print("Tanggal Mulai : ");
		String[] tglInput = input.nextLine().split(" ");

		System.out.print("Hari Libur : ");
		String[] libur = input.nextLine().split(", ");
		int n = libur.length;

		String bulan = "";
		String inputBulan = tglInput[1];
		for (int i = 0; i < listBulan.length; i++) {
			if (inputBulan.equals(listBulan[i])) {
				if (i < 9) {
					bulan = "0" + (i + 1);
				} else {
					bulan = "" + (i + 1);
				}
			}
		}

		String tgl = tglInput[0] + "-" + bulan + "-" + tglInput[2];
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date tanggal = format.parse(tgl);

		Calendar tglMulai = Calendar.getInstance();
		tglMulai.setTime(tanggal);
		Calendar tglSelesai = Calendar.getInstance();
		tglSelesai.setTime(tanggal);
		tglSelesai.add(Calendar.DATE, 10);

		Calendar cal = null;
		for (Date date = tglMulai.getTime(); tglMulai.before(tglSelesai); tglMulai.add(Calendar.DATE, 1)) {
			date = tglMulai.getTime();
			cal = Calendar.getInstance();
			cal.setTime(date);
			if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
					|| cal.get(Calendar.SATURDAY) == n || cal.get(Calendar.SUNDAY) == n) {
				tglSelesai.add(Calendar.DATE, 1);
			}
		}
		System.out.print("Pelatihan akan selesai tanggal : " + cal.get(Calendar.DATE) + " "
				+ listBulan[cal.get(Calendar.MONTH)] + " " + cal.get(Calendar.YEAR));

		input.close();
	}
}
