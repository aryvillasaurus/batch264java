package day3test;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class tesdate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		
		String tglPinjam = "09-06-2019";
        LocalDate datePinjam = LocalDate.parse(tglPinjam, format);
        System.out.println(datePinjam);
        
        String tglKembali = "10-07-2019";
        LocalDate dateKembali = LocalDate.parse(tglKembali, format);
        System.out.println(dateKembali);
        

        long periodeHari = ChronoUnit.DAYS.between(datePinjam, dateKembali);
        System.out.println(periodeHari + " hari");
        
        int denda = 0;
        if (periodeHari > 3) {
			denda = (int) (periodeHari * 500);
		} else {
			denda = 0;
		}      
        System.out.print("Denda :  " + denda);
	}

}
