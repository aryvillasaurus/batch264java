package day5;

import java.util.Scanner;

public class Main {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Enter the number of case");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;	
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}
	
	private static void soal1() {
		System.out.println("Soal 1 Berhasil");
	}
	
	private static void soal2() {
		System.out.println("Soal 2 Berhasil");
	}

}
