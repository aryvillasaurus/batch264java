package day5;

import java.util.*;

public class MainTestArray1Dim {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih nomor soal : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {
		System.out.println("Masukan n : ");
		int n = input.nextInt();
		int angka = 1;

		int[] nilaiGanjil = new int[n];

		for (int i = 0; i < n; i++) {
			nilaiGanjil[i] = angka;
			angka += 2;
			System.out.print(nilaiGanjil[i] + " ");
		}
	}

	private static void soal2() {
		int angka = 2;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiGenap = new int[n];

		for (int i = 0; i < n; i++) {
			nilaiGenap[i] = angka;
			angka += 2;
			System.out.println(nilaiGenap[i] + " ");
		}
	}

	private static void soal3() {
		int angka = 1;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiDeret = new int[n];

		for (int i = 0; i < n; i++) {
			nilaiDeret[i] = angka;
			angka += 3;
			System.out.println(nilaiDeret[i] + " ");
		}
	}

	private static void soal4() {
		int angka = 1;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiDeret = new int[n];

		for (int i = 0; i < n; i++) {
			nilaiDeret[i] = angka;
			angka += 4;
			System.out.println(nilaiDeret[i] + " ");
		}
	}

	private static void soal5() {
		int angka = 1;
		int reset = 0;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiDeret = new int[n + 1];

		for (int i = 1; i <= n; i++) {
			if (reset == 2) {
				nilaiDeret[i] = angka;
				System.out.print("* ");
				reset = 0;
			} else {
				nilaiDeret[i] = angka;
				angka += 4;
				System.out.print(nilaiDeret[i] + " ");
				reset += 1;
			}
			System.out.print(" ");
		}
	}

	private static void soal6() {
		int angka = 1;
		int reset = 0;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiDeret = new int[n + 1];

		for (int i = 1; i <= n; i++) {
			if (reset == 2) {
				nilaiDeret[i] = angka;
				angka += 4;
				System.out.print("* ");
				reset = 0;
			} else {
				nilaiDeret[i] = angka;
				angka += 4;
				System.out.print(nilaiDeret[i] + " ");
				reset += 1;
			}
			System.out.print(" ");
		}
	}

	private static void soal7() {
		int angka = 2;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiDeret = new int[n];

		for (int i = 0; i < n; i++) {
			nilaiDeret[i] = angka;
			angka *= 2;
			System.out.print(nilaiDeret[i] + " ");
		}
	}

	private static void soal8() {
		int angka = 3;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiDeret = new int[n];

		for (int i = 0; i < n; i++) {
			nilaiDeret[i] = angka;
			angka *= 3;
			System.out.print(nilaiDeret[i] + " ");
		}
	}

	private static void soal9() {
		int angka = 4;
		int reset = 0;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiDeret = new int[n + 1];

		for (int i = 1; i <= n; i++) {
			if (reset == 2) {
				nilaiDeret[i] = angka;
				System.out.print("* ");
				reset = 0;
			} else {
				nilaiDeret[i] = angka;
				angka *= 4;
				System.out.print(nilaiDeret[i] + " ");
				reset += 1;
			}
			System.out.print(" ");
		}
	}

	private static void soal10() {
		int nilaiAwal = 3;

		System.out.println("Masukan n : ");
		int n = input.nextInt();

		int[] nilaiDeret = new int[n];

		for (int i = 0; i < n; i++) {
			if (i == 3) {
				nilaiDeret[i] = nilaiAwal;
				nilaiAwal *= 3;
				System.out.print("XXX");
			} else {
				nilaiDeret[i] = nilaiAwal;
				nilaiAwal *= 3;
				System.out.print(nilaiDeret[i] + " ");
			}
			System.out.print(" ");
		}
	}

}
