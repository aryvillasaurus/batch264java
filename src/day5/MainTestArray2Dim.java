package day5;

import java.util.*;

public class MainTestArray2Dim {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih nomor soal : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				case 8:
					soal8();
					break;
				case 9:
					soal9();
					break;
				case 10:
					soal10();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {
		int nilaiAwal = 1;

		System.out.print("Masukan N : ");
		int n = input.nextInt();

		System.out.print("Masukan N2 : ");
		int n2 = input.nextInt();

		int banyakBaris = 2;

		int[][] arrayAngka = new int[banyakBaris][n];

		for (int i = 0; i < banyakBaris; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrayAngka[i][j] = j;
				} else {
					arrayAngka[i][j] = nilaiAwal;
					nilaiAwal *= n2;
				}
				System.out.print(arrayAngka[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static void soal2() {
		int nilaiAwal = 1;
		int reset = 0;

		System.out.print("Masukan N  : ");
		int n = input.nextInt();

		System.out.print("Masukan N2 : ");
		int n2 = input.nextInt();

		int banyakBaris = 2;

		int[][] arrayAngka = new int[banyakBaris][n];

		for (int i = 0; i < banyakBaris; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrayAngka[i][j] = j;
					System.out.print(arrayAngka[i][j] + " ");
				} else if (reset == 2) {
					arrayAngka[i][j] = nilaiAwal;
					nilaiAwal *= n2;
					System.out.print("-" + arrayAngka[i][j] + " ");
					reset = 0;
				} else {
					arrayAngka[i][j] = nilaiAwal;
					nilaiAwal *= n2;
					System.out.print(arrayAngka[i][j] + " ");
					reset += 1;
				}
			}
			System.out.println(" ");
		}
	}

	private static void soal3() {
		System.out.print("Masukan N : ");
		int n = input.nextInt();

		System.out.print("Masukan N2 : ");
		int n2 = input.nextInt();

		int banyakBaris = 2;

		int[][] arrayAngka = new int[banyakBaris][n];

		for (int i = 0; i < banyakBaris; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrayAngka[i][j] = j;
					System.out.print(arrayAngka[i][j] + " ");
				} else {
					if (n % 2 == 0) {
						arrayAngka[i][j] = n2;
						if (j < n / 2) {
							if (j == (n / 2) - 1) {
								arrayAngka[i][j] = n2;
								System.out.print(arrayAngka[i][j] + " ");
							} else {
								n2 *= 2;
								System.out.print(arrayAngka[i][j] + " ");
							}
						} else {
							n2 /= 2;
							System.out.print(arrayAngka[i][j] + " ");
						}
					} else {
						arrayAngka[i][j] = n2;
						if (j < n / 2) {
							arrayAngka[i][j] = n2;
							n2 *= 2;
							System.out.print(arrayAngka[i][j] + " ");
						} else {
							arrayAngka[i][j] = n2;
							n2 /= 2;
							System.out.print(arrayAngka[i][j] + " ");
						}
					}
				}

			}
			System.out.println();
		}
	}

	private static void soal4() {
		int angka2 = 0;
		int angkaAwal = 1;

		System.out.print("Masukan N : ");
		int n = input.nextInt();

		System.out.print("Masukan N2 : ");
		int n2 = input.nextInt();

		int banyakBaris = 2;

		int[][] arrayAngka = new int[banyakBaris][n];

		for (int i = 0; i < banyakBaris; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrayAngka[i][j] = j;
					System.out.print(arrayAngka[i][j] + " ");
				} else {
					arrayAngka[i][j] = angkaAwal;
					if (j % 2 == 0) {
						System.out.print(arrayAngka[i][j] + " ");
						angkaAwal += 1;
					} else {
						angka2 = angka2 + n2;
						System.out.print(angka2 + " ");
					}
				}
			}
			System.out.println("");
		}
	}

	private static void soal5() {
		int nilai = 0;
		int baris = 3;

		System.out.print("Masukan N :");
		int n = input.nextInt();
		System.out.println("=============");

		int array[][] = new int[baris][n];

		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < n; j++) {
				array[i][j] = nilai++;
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static void soal6() {
		int nilaiAwal1 = 0;
		int nilaiAwal2 = 1;

		System.out.print("Masukan N : ");
		int n = input.nextInt();

		int baris = 3;

		int[][] arrayAngka = new int[baris][n];

		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrayAngka[i][j] = nilaiAwal1;
					nilaiAwal1++;
					System.out.print(arrayAngka[i][j] + " ");
				} else if (i == 1) {
					arrayAngka[i][j] = nilaiAwal2;
					nilaiAwal2 *= n;
					System.out.print(arrayAngka[i][j] + " ");
				} else {
					arrayAngka[i][j] = arrayAngka[1][j] + arrayAngka[0][j];
					System.out.print(arrayAngka[i][j] + " ");
				}
			}
			System.out.println(" ");
		}
	}

	private static void soal7() {
		int nilai = 0;
		int baris = 3;

		System.out.print("Masukan N :");
		int n = input.nextInt();
		System.out.println("=============");

		int array[][] = new int[baris][n];

		for (int i = 0; i < baris; i++) {
			for (int j = 0; j < n; j++) {
				array[i][j] = nilai++;
				System.out.print(array[i][j] + " ");
			}
			System.out.println();
		}
	}

	private static void soal8() {
		int angka1 = 0;
		int angka2 = 0;
		int angka3 = 0;

		System.out.print("Masukan N : ");
		int n = input.nextInt();

		int banyakBaris = 3;

		int[][] arrayAngka = new int[banyakBaris][n];

		for (int i = 0; i < banyakBaris; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrayAngka[i][j] = angka1;
					angka1++;
					System.out.print(arrayAngka[i][j] + " ");
				} else if (i == 1) {
					arrayAngka[i][j] = angka2;
					angka2 += 2;
					System.out.print(arrayAngka[i][j] + " ");
				} else if (i == 2) {
					arrayAngka[i][j] = angka3;
					angka3 += 3;
					System.out.print(arrayAngka[i][j] + " ");
				}
			}
			System.out.println(" ");
		}
	}

	private static void soal9() {
		int angka1 = 0;
		int angka2 = 0;

		System.out.print("Masukan N : ");
		int n = input.nextInt();

		System.out.print("Masukan N2 : ");
		int n2 = input.nextInt();

		int banyakBaris = 3;

		int[][] arrayAngka = new int[banyakBaris][n];

		for (int i = 0; i < banyakBaris; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrayAngka[i][j] = angka1;
					angka1++;
					System.out.print(arrayAngka[i][j] + " ");
				} else if (i == 1) {
					arrayAngka[i][j] = angka2;
					angka2 += n2;
					System.out.print(arrayAngka[i][j] + " ");
				} else {
					arrayAngka[i][j] = arrayAngka[i - 1][n - 1 - j];
					System.out.print(arrayAngka[i][j] + " ");
				}
			}
			System.out.println(" ");
		}
	}

	private static void soal10() {
		int angka1 = 0;
		int angka2 = 0;

		System.out.print("Masukan N : ");
		int n = input.nextInt();

		System.out.print("Masukan N2 : ");
		int n2 = input.nextInt();

		int banyakBaris = 3;

		int[][] arrayAngka = new int[banyakBaris][n];

		for (int i = 0; i < banyakBaris; i++) {
			for (int j = 0; j < n; j++) {
				if (i == 0) {
					arrayAngka[i][j] = angka1;
					angka1++;
					System.out.print(arrayAngka[i][j] + " ");
				} else if (i == 1) {
					arrayAngka[i][j] = angka2;
					angka2 += n2;
					System.out.print(arrayAngka[i][j] + " ");
				} else if (i == 2) {
					arrayAngka[i][j] = arrayAngka[1][j] + arrayAngka[0][j];
					System.out.print(arrayAngka[i][j] + " ");
				}
			}
			System.out.println(" ");
		}
	}

}
