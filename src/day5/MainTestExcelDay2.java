package day5;

import java.util.Arrays;
import java.util.Scanner;

public class MainTestExcelDay2 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih nomor soal : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {
		int uangbayar = 0;
		int belanja = 0;
		int hargatotal = 0;

		System.out.print("Masukan Uang Pengunjung : ");
		uangbayar = input.nextInt();

		System.out.println("");
		System.out.println("=====================");
		System.out.print("Masukkan Jumlah Baju: ");

		int baju = input.nextInt();
		int hargabaju[] = new int[baju];

		int[] total_belanja = new int[9];

		for (int i = 0; i < baju; i++) {
			System.out.print("Masukan Harga Baju : ");
			hargabaju[i] = input.nextInt();
		}

		System.out.print("Masukkan Jumlah Celana: ");
		int celana = input.nextInt();
		int hargacelana[] = new int[celana];

		for (int i = 0; i < celana; i++) {
			System.out.print("Masukan Harga Celana: ");
			hargacelana[i] = input.nextInt();
		}

		for (int i = 0; i < hargabaju.length; i++) {
			for (int j = 0; j < hargacelana.length; j++) {
				hargatotal = hargabaju[i] + hargacelana[j];
				if (hargatotal <= uangbayar) {
					total_belanja[j] = hargatotal;
				}
				if (hargatotal <= uangbayar && hargatotal >= belanja) {
					belanja = hargatotal;
					Arrays.sort(total_belanja);
					System.out.println("=====================");
					System.out.print("Uang Belanja Maksimal : " + total_belanja[total_belanja.length - 1]);
				}
			}
		}
	}

	private static void soal2() {
		System.out.print("Masukan angka pada array: ");
		int n = input.nextInt();

		int array[] = new int[n];
		// Input data ke array
		for (int i = 0; i < array.length; i++) {
			System.out.print("angka ke " + (i + 1) + " : ");
			array[i] = input.nextInt();
		}
		// Menampilkan data dalam array
		System.out.println("Angka yang dimasukan ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		// rotasi
		System.out.print("Rotasi : ");
		int rotasi = input.nextInt();

		// cetak 1
		System.out.print("array awal     : ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();

		for (int rot = 0; rot < rotasi; rot++) {

			// geser
			int j = 0;
			int temp = array[0];
			for (j = 0; j < array.length - 1; j++) {
				array[j] = array[j + 1];
			}
			array[j] = temp;

			// cetak 2
			System.out.print("array geser " + (rot + 1) + "x : ");
			for (int i = 0; i < array.length; i++) {
				System.out.print(array[i] + ", ");
			}
			System.out.println();

		}
	}

	private static void soal3() {
		System.out.print("Masukan Banyaknya Puntung : ");
		int puntung = input.nextInt();
		int batang; 
		int sisa;
		int penghasilan;
		
		if (puntung/8 == 1) {
			batang = puntung/8;
			System.out.println("Rokok = "+ batang);
		}else {
			batang = puntung/8;
			sisa = puntung % 8;
			System.out.println("Sisa Puntung = "+ sisa);
			System.out.println("Rokok = "+ batang);
		}
		penghasilan = batang *500;
		System.out.println("Penghasilan : " +penghasilan);
	}
	
	private static void soal4() {
		int uangElsa;
		int sisaUang;
		int total = 0;
		int hasil = 0;
		int totalPatungan = 0;
		
		System.out.print("Masukkan jumlah menu: ");
		int jumlahMenu=input.nextInt();
		
		int arrayHarga[] = new int[jumlahMenu];
		
		System.out.print("Elsa alergi dengan menu ke: ");
		int alergi = input.nextInt();	

		for(int i=0; i<jumlahMenu; i++) {
			System.out.print("Masukkan harga menu: ");
			arrayHarga[i] = input.nextInt();
			if(i==alergi) {
				arrayHarga[i]=0;
			} else {
			total = hasil+arrayHarga[i];
			hasil = total;
			totalPatungan = total/2;
			}
		}
		
		System.out.print("Uang elsa: ");
		uangElsa = input.nextInt();

	    System.out.println("=============================\n");
		System.out.println("Elsa harus membayar = "+totalPatungan);
		sisaUang = uangElsa-totalPatungan;
		if(sisaUang>0) {
			System.out.println("Sisa uang elsa = "+sisaUang);
		}else if(sisaUang==0) {
			System.out.println("Uang pas");
		}else {
			System.out.println("Uang elsa kurang = "+sisaUang);
		}
	}
	
	private static void soal5() {
		System.out.print("Masukan Kata: ");
		String kata = input.nextLine();
		
		kata = kata.toLowerCase();
		
		char[] chars = kata.toCharArray();

		Arrays.sort(chars);
        String sorted = new String(chars);
		
		System.out.print("Huruf vokal yang muncul:");
		for (int i = 0; i < sorted.length(); i++) {
			if (sorted.charAt(i) == 'a' || sorted.charAt(i) == 'e' || sorted.charAt(i) == 'i' || sorted.charAt(i) == 'o'
					|| sorted.charAt(i) == 'u') {
				System.out.print("" + sorted.charAt(i));
			}
		}
		System.out.println("");
		
		System.out.print("Huruf konsonan yang muncul:");
		for (int j = 0; j < sorted.length(); j++) {	
			if (sorted.charAt(j) != 'a' && sorted.charAt(j) != 'e' && sorted.charAt(j) != 'i' && sorted.charAt(j) != 'o'
					&& sorted.charAt(j) != 'u' && sorted.charAt(j) != ' ') {
			
				System.out.print("" + sorted.charAt(j));
			}
		}
	}
	
	private static void soal6() {
		System.out.println("Masukan Kata : ");
		String[] kalimat = input.nextLine().split(" ");

		for (int i = 0; i < kalimat.length; i++) {
			char[] kata = kalimat[i].toCharArray();
			for (int j = 0; j < kata.length; j++) {
				if (j % 2 != 0) {
					kata[j] = '*';
				} else {
					kata[j] = kata[j];
				}
			}
			System.out.print(Arrays.toString(kata).replace("[", "").replace(",", "").replace("]", " "));
		}
	}
}
