package day5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MainTestExcelDay3 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih nomor soal : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				case 5:
					soal5();
					break;
				case 6:
					soal6();
					break;
				case 7:
					soal7();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {
		System.out.println("Masukkan angka N: ");
		int n = input.nextInt();

		int a = 1;
		int b = 1;

		System.out.println("Angka Fibonaci : ");
		for (int i = 1; i <= n; i++) {
			System.out.print(a + " ");
			int sum = a + b;
			a = b;
			b = sum;
		}
	}

	private static void soal2() {
		System.out.println("Masukkan angka N: ");
		int n = input.nextInt();

		int a = 1;
		int b = 1;
		int c = 1;

		System.out.println("Angka Fibonaci : ");
		for (int i = 1; i <= n; i++) {
			System.out.print(a + " ");
			int sum = a + b + c;
			a = b;
			b = c;
			c = sum;
		}
	}

	private static void soal3() {
		System.out.print("Masukkan nilai N : ");
		int n = input.nextInt();

		System.out.println("Bilangan Prima : ");
		for (int i = 1; i < n; i++) {
			int bil = 0;
			for (int j = 1; j <= n; j++) {
				if (i % j == 0) {
					bil++;
				}
			}
			if ((bil == 2)) {
				System.out.print(i + " ");
			}
		}
	}

	private static void soal4() throws ParseException {
		System.out.print("Waktu = ");
		String inputJam = input.nextLine();
		SimpleDateFormat tanggal = new SimpleDateFormat("hh:mm:ssaa");
		SimpleDateFormat outputformat = new SimpleDateFormat("HH:mm:ss");
		Date waktu = null;
		String str = "";

		waktu = tanggal.parse(inputJam);
		str = outputformat.format(waktu);
		System.out.println("Waktu = " + str);
	}

	private static void soal5() {
		System.out.println("Masukkan N : ");
		int nilai = input.nextInt();

		int nilaiBaru = nilai;
		System.out.println("Faktor-faktornya:");
		for (int i = 1; i <= nilai; i++) {
			for (int j = 2; j <= nilai; j++) {
				if (nilaiBaru % j == 0) {
					System.out.println(nilaiBaru + "/" + j + "=" + (nilaiBaru / j));
					nilaiBaru /= j;
					break;
				}
			}
		}
	}

	private static void soal6() {
		double[] arrayJarak = { 2.0, 0.5, 1.5, 0.3 };
		double jarak = 0;

		System.out.println("Ke berapa tempat supir ojol pergi : ");
		int x = input.nextInt();
		int tujuan[] = new int[x + 1];
		for (int i = 0; i < x; i++) {
			System.out.print("Tujuan supir ojol : ");
			tujuan[i + 1] = input.nextInt();
			jarak = jarak + arrayJarak[tujuan[i]];
		}
		System.out.println("Jarak tempuh= " + jarak);
		double bensin = jarak / 2.5;
		int ibensin = (int) bensin;
		double pembulatan = bensin - (double) ibensin;
		if (pembulatan > 0) {
			ibensin += 1;
		}
		System.out.println("Bensin = " + ibensin);
	}

	private static void soal7() {
		System.out.println("SOS : ");
		String sinyal = input.nextLine().toLowerCase();
		char[] cek = new char[sinyal.length()];

		int n = sinyal.length();
		int tampung = n / 3;

		for (int i = 0; i < n; i++) {
			cek[i] = sinyal.charAt(i);
		}
		for (int i = 0; i < n; i += 3) {
			if (cek[i] == 's' && cek[i + 1] == 'o' && cek[i + 2] == 's') {
				tampung--;
			}
		}
		System.out.println("Total sinyal salah : " + tampung);
	}

}
