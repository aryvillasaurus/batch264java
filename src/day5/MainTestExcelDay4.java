package day5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class MainTestExcelDay4 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih nomor soal : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
					break;
				case 3:
					soal3();
					break;
				case 4:
					soal4();
					break;
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() throws ParseException {
		System.out.println("Masukan Tanggal Masuk :");
		String tglMasuk = input.nextLine();

		System.out.print("Masukan Jam Masuk: ");
		String waktuMasuk = input.next();
		System.out.print("Masukan Jam Keluar: ");
		String waktuKeluar = input.next();

		SimpleDateFormat format = new SimpleDateFormat("hh.mm.ss");
		Date mulai = format.parse(waktuMasuk + ".00");
		Date keluar = format.parse(waktuKeluar + ".00");

		int biayaParkir = 0;
		long lamaDalamMenit = (keluar.getTime() - mulai.getTime()) / (1000 * 60);
		float hitung = (float) lamaDalamMenit / 60;
		if (hitung % 1 >= 0.5) {
			biayaParkir += (int) Math.ceil(hitung) * 3000;
		} else {
			biayaParkir += hitung * 3000;
		}
		System.out.println("Biaya Parkir = " + biayaParkir);
	}

	private static void soal2() throws ParseException {
		System.out.print("Masukan Tanggal Pinjam : ");
		String tglPinjam = input.next();
		System.out.print("Masukan Tanggal Kembali: ");
		String tglKembali = input.next();

		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date pinjam = format.parse(tglPinjam);
		Date kembali = format.parse(tglKembali);

		int denda = 0;
		int lamaTgl = kembali.getDate() - pinjam.getDate();
		int lamaBulan = kembali.getMonth() - pinjam.getMonth();
		int lamaTahun = kembali.getYear() - pinjam.getYear();

		int lamaPinjam = lamaTgl + (lamaBulan * 30) + (lamaTahun * 365);

		if (lamaPinjam > 3) {
			denda = lamaPinjam * 500;
		} else {
			denda = 0;
		}

		System.out.println("Lama Peminjaman : " + lamaPinjam + " Hari");
		System.out.print("Denda :  " + denda);
	}

	private static void soal3() throws ParseException {
		String[] listBulan = { "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September",
				"Oktober", "November", "Desember" };

		System.out.print("Tanggal Mulai : ");
		String[] tglInput = input.nextLine().split(" ");

		System.out.print("Hari Libur : ");
		String[] libur = input.nextLine().split(", ");

		String bulan = "";
		String inputBulan = tglInput[1];
		for (int i = 0; i < listBulan.length; i++) {
			if (inputBulan.equals(listBulan[i])) {
				if (i < 9) {
					bulan = "0" + (i + 1);
				} else {
					bulan = "" + (i + 1);
				}
			}
		}

		String tgl = tglInput[0] + "-" + bulan + "-" + tglInput[2];
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date tanggal = format.parse(tgl);

		Calendar tglMulai = Calendar.getInstance();
		tglMulai.setTime(tanggal);
		Calendar tglSelesai = Calendar.getInstance();
		tglSelesai.setTime(tanggal);
		tglSelesai.add(Calendar.DATE, 10);

		Calendar cal = null;
		for (Date date = tglMulai.getTime(); tglMulai.before(tglSelesai); tglMulai.add(Calendar.DATE, 1)) {
			date = tglMulai.getTime();
			cal = Calendar.getInstance();
			cal.setTime(date);
			if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
					|| cal.get(Calendar.SATURDAY) == libur.length || cal.get(Calendar.SUNDAY) == libur.length) {
				tglSelesai.add(Calendar.DATE, 1);
			}
		}
		System.out.print("Pelatihan akan selesai tanggal : " + cal.get(Calendar.DATE) + " "
				+ listBulan[cal.get(Calendar.MONTH)] + " " + cal.get(Calendar.YEAR));
	}

	private static void soal4() {
		String balik = "";

		System.out.println("Masukan Kata : ");
		String kata = input.nextLine();

		int length = kata.length();
		for (int i = length - 1; i >= 0; i--)
			balik = balik + kata.charAt(i);

		System.out.println("Kata balikan :" + balik);

		if (kata.equals(balik))
			System.out.println("Yes");
		else
			System.out.println("No");
	}

}
