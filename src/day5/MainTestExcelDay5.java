package day5;

import java.util.Arrays;
import java.util.*;

public class MainTestExcelDay5 {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String answer = "Y";

		while (answer.toUpperCase().equals("Y")) {
			try {
				System.out.println("Pilih nomor soal : ");
				int number = input.nextInt();
				input.nextLine();

				switch (number) {
				case 1:
					soal1();
					break;
				case 2:
					soal2();
				default:
					System.out.println("Case is not available");
					break;
				}

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			answer = input.next();
		}
	}

	private static void soal1() {

		String jawab = "Y";

		while (jawab.toUpperCase().equals("Y")) {
			try {
				int angkaRandom = (int) (Math.random() * 9);

				System.out.println("Point : ");
				int point = input.nextInt();

				System.out.println("Taruhan :");
				int taruhan = input.nextInt();

				System.out.println("Masukan Tebakan U/D :");
				String tebak = input.next();

				System.out.println("Angka yang keluar : " + angkaRandom);

				if (angkaRandom > 5 && tebak.toUpperCase().equals("U")) {
					System.out.println("You Win!");
					point += taruhan;
				} else if (angkaRandom <= 5 && tebak.toUpperCase().equals("U")) {
					System.out.println("You Lose");
					point -= taruhan;
				} else if (angkaRandom <= 5 && tebak.toUpperCase().equals("D")) {
					System.out.println("You Win!");
					point += taruhan;
				} else if (angkaRandom > 5 && tebak.toUpperCase().equals("D")) {
					System.out.println("You Lose!");
					point -= taruhan;
				}
				System.out.println("Point Anda Sekarang : " + point);

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			jawab = input.next();
		}

	}

	private static void soal2() {
		System.out.println("Masukan angka : ");
		String[] arrayAngka = input.next().split(",");
		int n = arrayAngka.length;
		int arrayDeretInt[] = new int[n];

		for (int i = 0; i < n; i++) {
			arrayDeretInt[i] = Integer.parseInt(arrayAngka[i]);
		}
		String urut = "";
		Arrays.sort(arrayDeretInt);

		for (int i = 0; i < n; i++) {
			urut += arrayDeretInt[i] + " ";
		}
		System.out.println("Angka yang diurut : " + urut);

		int totalAngkaTerkecil = 0;
		int totalAngkaTerbesar = 0;

		for (int i = 0; i < 3; i++) {
			totalAngkaTerkecil += arrayDeretInt[i];
			totalAngkaTerbesar += arrayDeretInt[n - 1 - i];
		}

		System.out.println("=========================");
		System.out.println("Total 3 Angka Terkecil : " + totalAngkaTerkecil);
		System.out.println("Total 3 Angka Terbesar : " + totalAngkaTerbesar);
	}

}
