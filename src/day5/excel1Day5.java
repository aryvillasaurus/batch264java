package day5;

import java.util.*;

public class excel1Day5 {
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String jawab = "Y";

		while (jawab.toUpperCase().equals("Y")) {
			try {
				Scanner input = new Scanner(System.in);
				int angkaRandom = (int) (Math.random() * 9);

				System.out.println("Point : ");
				int point = input.nextInt();

				System.out.println("Taruhan :");
				int taruhan = input.nextInt();
				
				System.out.println("Masukan Tebakan U/D :");
				String tebak = input.next();
				
				System.out.println("Angka yang keluar : " + angkaRandom);

				if (angkaRandom > 5 && tebak.toUpperCase().equals("U")) {
					System.out.println("You Win!");
					point += taruhan;
				} else if (angkaRandom <= 5 && tebak.toUpperCase().equals("U")) {
					System.out.println("You Lose");
					point -= taruhan;
				} else if (angkaRandom <= 5 && tebak.toUpperCase().equals("D")) {
					System.out.println("You Win!");
					point += taruhan;
				} else if (angkaRandom > 5 && tebak.toUpperCase().equals("D")) {
					System.out.println("You Lose!");
					point -= taruhan;
				}
			System.out.println("Point Anda Sekarang : " + point);				

			} catch (Exception error) {
				System.out.println(error.getMessage());
			}
			System.out.println();
			System.out.println("Continue?");
			jawab = input.next();
		}

	}
}
