package day5;

import java.util.*;

public class excel2Day5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		System.out.println("Masukan angka : ");
		String[] arrayAngka = input.next().split(",");
		int n = arrayAngka.length;
		int arrayDeretInt[] = new int[n];

		for (int i = 0; i < n; i++) {
			arrayDeretInt[i] = Integer.parseInt(arrayAngka[i]);
		}
		String urut = "";
		Arrays.sort(arrayDeretInt);

		for (int i = 0; i < n; i++) {
			urut += arrayDeretInt[i] + " ";
		}
		System.out.println("Angka yang diurut : " + urut);

		int totalAngkaTerkecil = 0;
		int totalAngkaTerbesar = 0;

		for (int i = 0; i < 3; i++) {
			totalAngkaTerkecil += arrayDeretInt[i];
			totalAngkaTerbesar += arrayDeretInt[n - 1 - i];
		}

		System.out.println("=========================");
		System.out.println("Total 3 Angka Terkecil : " + totalAngkaTerkecil);
		System.out.println("Total 3 Angka Terbesar : " + totalAngkaTerbesar);
	}

}
