package day6;

import java.util.*;

public class excelpr {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		// input banyak angka
		System.out.print("Masukan Jumlah Angka: ");
		int n = input.nextInt();
		int[] arrayAngka = new int[n];

		// input angka
		System.out.println("Masukan Angka : ");
		for (int i = 0; i < n; i++) {
			arrayAngka[i] = input.nextInt();
		}
		String urut = "";
		Arrays.sort(arrayAngka);
		for (int i = 0; i < n; i++) {
			urut += arrayAngka[i] + " ";
		}

		// hitung mean
		double mean = 0;
		for (double angka : arrayAngka) {
			mean = mean + angka / n;
		}

		System.out.println("========================");
		System.out.println("Angka yang Ditampilkan: \n" + urut);
		System.out.println("=========================");

		System.out.printf("Mean: %s : %d = %.2f \n",
				Arrays.toString(arrayAngka).replace("[", "(").replace(",", " +").replace("]", ")"), n, mean);

		double median;
		if (arrayAngka.length % 2 == 0) {
			median = ((double) arrayAngka[arrayAngka.length / 2] + (double) arrayAngka[arrayAngka.length / 2 - 1]) / 2;
			System.out.println("Median : " + "(" + arrayAngka[arrayAngka.length / 2] + " + "
					+ arrayAngka[arrayAngka.length / 2 - 1] + ")" + "/2" + " = " + median);
		} else {
			median = (double) arrayAngka[arrayAngka.length / 2];
			System.out.println("Median : " + "(" + arrayAngka[arrayAngka.length / 2] + " + "
					+ arrayAngka[arrayAngka.length / 2 - 1] + ")" + "/2" + " = " + median);
		}
		// hitung modus
		int maxModus = 0;
		int maxCount = 0;
		for (int i = 0; i < arrayAngka.length; ++i) {
			int count = 0;
			for (int j = 0; j < arrayAngka.length; ++j) {
				if (arrayAngka[j] == arrayAngka[i])
					++count;
			}
			if (count > maxCount) {
				maxCount = count;
				maxModus = arrayAngka[i];

			}

		}
		System.out.println("Modus: " + maxModus);
	}

}