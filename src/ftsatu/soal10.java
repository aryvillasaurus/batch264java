package ftsatu;

import java.util.Arrays;
import java.util.Scanner;

public class soal10 {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		
		System.out.print("Masukan Kata: ");
		String kata = input.nextLine();
		
		kata = kata.toLowerCase();
		
		char[] chars = kata.toCharArray();

		Arrays.sort(chars);
        String sorted = new String(chars);
		
		System.out.print("Huruf vokal yang muncul:");
		for (int i = 0; i < sorted.length(); i++) {
			if (sorted.charAt(i) == 'a' || sorted.charAt(i) == 'e' || sorted.charAt(i) == 'i' || sorted.charAt(i) == 'o'
					|| sorted.charAt(i) == 'u') {
				System.out.print("" + sorted.charAt(i));
			}
		}
		System.out.println("");
		
		System.out.print("Huruf konsonan yang muncul:");
		for (int j = 0; j < sorted.length(); j++) {	
			if (sorted.charAt(j) != 'a' && sorted.charAt(j) != 'e' && sorted.charAt(j) != 'i' 
					&& sorted.charAt(j) != 'o' && sorted.charAt(j) != 'u' && sorted.charAt(j) != '!') {
				System.out.print("" + sorted.charAt(j));
			}
		}
		System.out.println("");
		
		System.out.print("Karakter selain vokal dan konsonan yang muncul:");
		for (int k = 0; k < sorted.length(); k++) {	
			if (sorted.charAt(k) == '1' || sorted.charAt(k) == '2' || sorted.charAt(k) == '3' || sorted.charAt(k) == '4' || sorted.charAt(k) == '5'
					|| sorted.charAt(k) == '6' || sorted.charAt(k) == '7' || sorted.charAt(k) == '8' || sorted.charAt(k) == '9' || sorted.charAt(k) == '0'
					|| sorted.charAt(k) == '!' || sorted.charAt(k) == '@' || sorted.charAt(k) == '#' || sorted.charAt(k) == '$' || sorted.charAt(k) == '%'
					|| sorted.charAt(k) == '^' || sorted.charAt(k) == '&' || sorted.charAt(k) == '*' || sorted.charAt(k) == '(' || sorted.charAt(k) == ')'
					|| sorted.charAt(k) == '_' || sorted.charAt(k) != '-' || sorted.charAt(k) == '+' || sorted.charAt(k) == '=' || sorted.charAt(k) == ']'
					|| sorted.charAt(k) == '[' && sorted.charAt(k) != 'a' || sorted.charAt(k) != 'e' || sorted.charAt(k) != 'i' 
					|| sorted.charAt(k) != 'o' || sorted.charAt(k) != 'u' || sorted.charAt(k) != ' ') {
				System.out.print("" + sorted.charAt(k));
			}
		}
	}
}
