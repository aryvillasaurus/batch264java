package ftsatu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class soal4 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.print("Waktu = ");
		String[] dataWaktu = input.next().split(":");
		String jam = dataWaktu[0];
		String menit = dataWaktu[1].substring(0, 2);
		String waktu = dataWaktu[1].substring(2, 4);

		
		if(waktu.equals("AM")) {
			if (jam.equals("12")) {
				jam = "00";
			}
			System.out.println(jam + ":" + menit);
		}else {
			if(!jam.equals("12")) {
				int h = Integer.parseInt(jam);
				h = h + 12;
				jam = ""+h;
			}
			System.out.println(jam + ":" + menit);
		}
		input.close();
	}
}
