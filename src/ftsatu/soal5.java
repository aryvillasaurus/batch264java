package ftsatu;

import java.util.Scanner;

public class soal5 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		String[] huruf = {"a","b","c","d","e","f","g","h",
							"i","j","k","l","m","n","o","p",
							"r","s","t","u","v","w","x","y","z"};
		
		System.out.println("Masukan Kalimat: ");
		String[] kalimat = input.nextLine().replace(" ", "").split("");
	
		int[] angka = new int[kalimat.length];
		
		for (int i= 0; i < kalimat.length; i++) {
			System.out.print("Masukan angka : ");
			angka[i] = input.nextInt();
		}
		System.out.println();
		System.out.println("Hasil : ");
		for (int i = 0; i < kalimat.length; i++) {
			if(kalimat[i].equals(huruf[angka[i]-1])) {
				System.out.print("True, ");
			} else {
				System.out.print("False, ");
			}
		}		
		input.close();
	}
}
