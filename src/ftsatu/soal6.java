package ftsatu;

import java.util.Scanner;

public class soal6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int reset = 0;

		System.out.println("Masukan N : ");
		int n = input.nextInt();
		int[] nilaiDeret = new int[n+1];

		for (int i = 0; i <= n; i++) {
			nilaiDeret[i] += i;
			if (reset == 2 && nilaiDeret[i] % 2 == 0 && nilaiDeret[i] % 3 == 0) {
				System.out.print("! ");
				reset = 0;
			} else if (nilaiDeret[i] % 2 == 0 && nilaiDeret[i] % 3 == 0){
				System.out.print(nilaiDeret[i] +" ");
				reset+=1;
			}
		}
		System.out.print("");
	}

}
