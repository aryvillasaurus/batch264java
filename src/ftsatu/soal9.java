package ftsatu;

import java.util.Scanner;

public class soal9 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner scanner = new Scanner(System.in);
		System.out.println("Masukkan angka N: ");
		int n = scanner.nextInt();
		
		int[] fibo = new int[n];
		
		fibo[0] = 1;
		fibo[1] = 1;
		int temp = 0;
		
		for(int i = 2; i < n; i++) {
			fibo[i] = fibo[i-1] + fibo[i-2];
		}
		
		System.out.println("Angka Fibonaci Angka Genap: ");
		for(int i = 0; i < n; i++) {
			if(fibo[i] >= n) {
				break;
			}
			if(fibo[i]%2 == 0) {
				temp+=1;
				System.out.print(fibo[i] + " ");
			}
		}
		System.out.println();
		System.out.println("Jumlah Angka Genap : " + temp);		
	}

}
