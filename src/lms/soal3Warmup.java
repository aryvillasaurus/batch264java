package lms;

import java.util.Arrays;
import java.util.Scanner;

public class soal3Warmup {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);

		// input banyak angka
		System.out.print("Masukan Jumlah Angka: ");
		int n = input.nextInt();
		int[] arrayAngka = new int[n];

		// input angka
		System.out.println("Masukan Angka : ");
		for (int i = 0; i < n; i++) {
			arrayAngka[i] = input.nextInt();
		}

		Arrays.sort(arrayAngka);

		int sum = 0;
		for (double angka : arrayAngka) {
			sum += angka;
		}

		System.out.println("Array Sum : "
				+ Arrays.toString(arrayAngka).replace("[", "").replace(",", " +").replace("]", "") + " = " + sum);
	}

}
