package lms;

import java.util.Arrays;
import java.util.Scanner;

public class soal7Warmup {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		
		System.out.println("Masukan angka :");
		String [] arrayAngka = input.nextLine().split(" ");
		int[] arrayInt = new int[arrayAngka.length];
		int[] arrayB = new int[arrayAngka.length];
		
		for(int i = 0; i < arrayInt.length; i++) {
			arrayInt[i] = Integer.parseInt(arrayAngka[i]);
		}
		
		int sum = 0;
		for (int i = 0; i < arrayAngka.length; i++)	{
			for (int j = 0; j < arrayAngka.length; j++)	{
				sum += arrayInt[j];
			}
			arrayB[i] = sum - arrayInt[i];
			sum = 0;
		}
		Arrays.sort(arrayB);
		System.out.print(arrayB[0] + " ");
		System.out.print(arrayB[arrayB.length-1]);
	}

}
